/****************************************************************************
 *
 *   Copyright (c) 2019 MyDRONE Development Team. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name mydrone nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

# Use native toolchain and disable THUMB mode for unit tests
override ARM_SDK_PREFIX :=
override THUMB :=

GTEST_SRC_DIR := $(GTEST_DIR)/src

# Unit test source files
ALLSRC     := $(SRC) $(wildcard ./*.c)
ALLCPPSRC  := $(wildcard ./*.cpp) $(GTEST_SRC_DIR)/gtest_main.cc
ALLSRCBASE := $(notdir $(basename $(ALLSRC) $(ALLCPPSRC)))
ALLOBJ     := $(addprefix $(OUTDIR)/, $(addsuffix .o, $(ALLSRCBASE)))

$(foreach src,$(ALLSRC),$(eval $(call COMPILE_C_TEMPLATE,$(src))))
$(foreach src,$(ALLCPPSRC),$(eval $(call COMPILE_CXX_TEMPLATE,$(src))))

# Specific extensions to CPPFLAGS only for the google test library
$(OUTDIR)/gtest-all.o: CPPFLAGS += -I$(GTEST_SRC_DIR)/..

$(eval $(call COMPILE_CXX_TEMPLATE, $(GTEST_SRC_DIR)/gtest-all.cc))
$(eval $(call LINK_CXX_TEMPLATE,$(OUTDIR)/$(TARGET).elf,$(ALLOBJ) $(OUTDIR)/gtest-all.o))

# Flags passed to the preprocessor
CPPFLAGS += -I$(GTEST_DIR)/include

# Flags passed to the C++ compiler
CXXFLAGS += -g -Wall -Wextra -Wno-missing-field-initializers

# Flags passed to the C compiler
CONLYFLAGS += -std=gnu99

# UNIT_TEST allows to for example to have optional test fixture code enabled or private code exposed in application modules. 
CFLAGS += -DUNIT_TEST
CPPFLAGS += -DUNIT_TEST

# Common compiler flags
CFLAGS += -O0 -g
CFLAGS += -Wall -Werror
CFLAGS += $(patsubst %,-I%,$(EXTRAINCDIRS))

# Google Test needs the pthread library
LDFLAGS += -lpthread

.PHONY: elf
elf: $(OUTDIR)/$(TARGET).elf

.PHONY: xml
xml: $(OUTDIR)/test-reports/$(TARGET).xml

$(OUTDIR)/test-reports/$(TARGET).xml: $(OUTDIR)/$(TARGET).elf
	$(V0) @echo " TEST XML  $(MSG_EXTRA)  $(call toprel, $@)"
	$(V1) $< --gtest_output=xml:$@ > /dev/null

.PHONY: run
run: $(OUTDIR)/$(TARGET).elf
	$(V0) @echo " TEST RUN  $(MSG_EXTRA)  $(call toprel, $<)"
	$(V1) $<
