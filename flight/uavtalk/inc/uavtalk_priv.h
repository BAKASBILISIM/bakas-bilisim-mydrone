/****************************************************************************
 *
 *   Copyright (c) 2019 MyDRONE Development Team. All rights reserved.
 *   Copyright (c) 2019 BAKAS BILISIM. All rights reserved.
 *   http://www.compositeware.com/ebc.html
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name mydrone nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#ifndef UAVTALK_PRIV_H
#define UAVTALK_PRIV_H

#include "uavobjectsinit.h"

// Private types and constants

// min header : sync(1), type (1), size(2), object ID(4), instance ID(2)
#define UAVTALK_MIN_HEADER_LENGTH  10

// max header : sync(1), type (1), size(2), object ID(4), instance ID(2), timestamp(2)
#define UAVTALK_MAX_HEADER_LENGTH  12

#define UAVTALK_CHECKSUM_LENGTH    1

#define UAVTALK_MAX_PAYLOAD_LENGTH (UAVOBJECTS_LARGEST + 1)

#define UAVTALK_MIN_PACKET_LENGTH  UAVTALK_MAX_HEADER_LENGTH + UAVTALK_CHECKSUM_LENGTH
#define UAVTALK_MAX_PACKET_LENGTH  UAVTALK_MIN_PACKET_LENGTH + UAVTALK_MAX_PAYLOAD_LENGTH

typedef struct {
    uint8_t  type;
    uint16_t packet_size;
    uint32_t objId;
    uint16_t instId;
    uint32_t length;
    uint8_t  timestampLength;
    uint8_t  cs;
    uint16_t timestamp;
    uint32_t rxCount;
    UAVTalkRxState state;
    uint16_t rxPacketLength;
} UAVTalkInputProcessor;

typedef struct {
    uint8_t canari;
    UAVTalkOutputStream outStream;
    xSemaphoreHandle    lock;
    xSemaphoreHandle    transLock;
    xSemaphoreHandle    respSema;
    uint8_t      respType;
    uint32_t     respObjId;
    uint16_t     respInstId;
    UAVTalkStats stats;
    UAVTalkInputProcessor iproc;
    uint8_t      *rxBuffer;
    uint8_t      *txBuffer;
} UAVTalkConnectionData;

#define UAVTALK_CANARI          0xCA
#define UAVTALK_SYNC_VAL        0x3C

#define UAVTALK_TYPE_MASK       0x78
#define UAVTALK_TYPE_VER        0x20
#define UAVTALK_TIMESTAMPED     0x80
#define UAVTALK_TYPE_OBJ        (UAVTALK_TYPE_VER | 0x00)
#define UAVTALK_TYPE_OBJ_REQ    (UAVTALK_TYPE_VER | 0x01)
#define UAVTALK_TYPE_OBJ_ACK    (UAVTALK_TYPE_VER | 0x02)
#define UAVTALK_TYPE_ACK        (UAVTALK_TYPE_VER | 0x03)
#define UAVTALK_TYPE_NACK       (UAVTALK_TYPE_VER | 0x04)
#define UAVTALK_TYPE_OBJ_TS     (UAVTALK_TIMESTAMPED | UAVTALK_TYPE_OBJ)
#define UAVTALK_TYPE_OBJ_ACK_TS (UAVTALK_TIMESTAMPED | UAVTALK_TYPE_OBJ_ACK)

// macros
#define CHECKCONHANDLE(handle, variable, failcommand) \
    variable = (UAVTalkConnectionData *)handle; \
    if (variable == NULL || variable->canari != UAVTALK_CANARI) { \
        failcommand; \
    }

#endif // UAVTALK__PRIV_H
/**
 * @}
 * @}
 */
