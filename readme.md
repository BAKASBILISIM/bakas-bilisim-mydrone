MyDRONE is the first stage in our larger project MyWORLD which aims to assess Global Warming via environmental air and water pollution measurements with unbiased data generation that relies on drones utlized as roaming measurement nodes.

Our latest R&D project MyWORLD uses drones (MyDRONE) to assess air pollution.
The obvious advantage over existing stationary stations is the flexibity of airbone measurements.
			
To draw attention to our project and to provide children with first hand insight to new technologies, the first 5 prototypes have been donated to 5 schools in Ankara.
			
The flight paths of these initial test runs are planned by children. MyDRONEs equipped with gas sensors take off from the school yard at noon where all interested students can partipate in the activity.
After collecting hazardous gas data, MyDRONEs return automatically to the school yard where students are encouraged to download the data to our main server.
			
The flight paths, the schools that participate in the project and the air pollution assessment data are going to be published on a dedicated web site soon. 
			
Goals of MyWORLD
	2020 Goal : 300 MyDRONEs, 180 Primary Schools, 120 High Schools, 100.000 Volunteers, Turkey only
	2021 Goal : 5000 MyDRONEs, 3000 Primary Schools, 2000 High Schools, 1.666.000 Volunteers, 10 countries

New Technologies that are introduced to children by MyWORLD: 
			Drones
			Microcontrollers
			Gas Sensors
			Flight Planning   
			Data Scienc

As a result of our R&D we have integrated hazardous gas sensors to a drone architecture resulting in airbourne measurement nodes.
Our R&D efforts are focused on designing very cheap air pollution detectors.
We have met our goal in this project. Each of our measuring nodes are only 1/20th the price of a stationary measurement station.

The next phases in our MyDRONE projects are as follows:
	Phase 1 of project : Working on the completion of the Air Pollution Assessment 
	Phase 2 of project : Inital design for the Global Warming Assessment
	Phase 3 of project : Draft design for the Water Pollution Assessment