/****************************************************************************
 *
 *   Copyright (c) 2019 MyDRONE Development Team. All rights reserved.
 *   Copyright (c) 2019 BAKAS BILISIM. All rights reserved.
 *   http://www.compositeware.com/ebc.html
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name mydrone nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/
# copy legacy OpenGL DLL

TEMPLATE = aux

include(gcs.pri)

MESAWIN_DIR = $$(MESAWIN_DIR)
isEmpty(MESAWIN_DIR):MESAWIN_DIR = $${TOOLS_DIR}/mesawin

# opengl32.dll will be copied to ./bin/opengl32/ for the installer to use
# the installer packages the dll and optionally allows to install it to ./bin/
# this implies that the opengl32.dll will not be used in a dev environment,
# unless it is copied to the ./bin/ ...

exists($${MESAWIN_DIR}) {
    contains(QT_ARCH, i386) {
        # take opengl32.dll from mesa (32 bit)
        OPENGL_DIR = $${MESAWIN_DIR}/opengl32_32
    }
    else {
        # take opengl32.dll from mesa (64 bit)
        OPENGL_DIR = $${MESAWIN_DIR}/opengl32_64
    }
}
else {
    # take opengl32.dll from mingw32/bin/
    # WARNING : doesn't currently work, GCS crashes at startup when using msys2 opengl32.dll
    warning("msys2 opengl32.dll breaks GCS, please install mesa with: make mesawin_install")
    # skip installing opengl32.dll, the package target will fail when creating the installer
    #OPENGL_DIR = $$[QT_INSTALL_BINS]
}

OPENGL_DLLS = \
    opengl32.dll

exists($${OPENGL_DIR}):for(dll, OPENGL_DLLS) {
    addCopyFileTarget($${dll},$${OPENGL_DIR},$${GCS_APP_PATH}/opengl32)
}
