/****************************************************************************
 *
 *   Copyright (c) 2019 MyDRONE Development Team. All rights reserved.
 *   Copyright (c) 2019 BAKAS BILISIM. All rights reserved.
 *   http://www.compositeware.com/ebc.html
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name mydrone nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/
 

#ifndef UAVOBJECTPARSER_H
#define UAVOBJECTPARSER_H

#include <QString>
#include <QStringList>
#include <QList>
#include <QDomNode>
#include <QByteArray>

// Types
typedef enum {
    FIELDTYPE_INT8 = 0,
    FIELDTYPE_INT16,
    FIELDTYPE_INT32,
    FIELDTYPE_UINT8,
    FIELDTYPE_UINT16,
    FIELDTYPE_UINT32,
    FIELDTYPE_FLOAT32,
    FIELDTYPE_ENUM
} FieldType;

typedef struct {
    QString   name;
    QString   description;
    QString   units;
    FieldType type;
    int         numElements;
    int         numOptions;
    int         numBytes;
    QStringList elementNames;
    QStringList options; // for enums only
    bool        defaultElementNames;
    QStringList defaultValues;
    QString     limitValues;
} FieldInfo;

/**
 * Object update mode
 */
typedef enum {
    UPDATEMODE_MANUAL    = 0, /** Manually update object, by calling the updated() function */
    UPDATEMODE_PERIODIC  = 1, /** Automatically update object at periodic intervals */
    UPDATEMODE_ONCHANGE  = 2, /** Only update object when its data changes */
    UPDATEMODE_THROTTLED = 3 /** Object is updated on change, but not more often than the interval time */
} UpdateMode;


typedef enum {
    ACCESS_READWRITE = 0,
    ACCESS_READONLY  = 1
} AccessMode;


typedef struct  {
    QString    name;
    QString    namelc; /** name in lowercase */
    QString    filename;
    quint32    id;
    bool       isSingleInst;
    bool       isSettings;
    bool       isPriority;
    AccessMode gcsAccess;
    AccessMode flightAccess;
    bool       flightTelemetryAcked;
    UpdateMode flightTelemetryUpdateMode; /** Update mode used by the autopilot (UpdateMode) */
    int flightTelemetryUpdatePeriod; /** Update period used by the autopilot (only if telemetry mode is PERIODIC) */
    bool       gcsTelemetryAcked;
    UpdateMode gcsTelemetryUpdateMode; /** Update mode used by the GCS (UpdateMode) */
    int gcsTelemetryUpdatePeriod; /** Update period used by the GCS (only if telemetry mode is PERIODIC) */
    UpdateMode loggingUpdateMode; /** Update mode used by the logging module (UpdateMode) */
    int loggingUpdatePeriod; /** Update period used by the logging module (only if logging mode is PERIODIC) */
    QList<FieldInfo *> fields; /** The data fields for the object **/
    QString    description; /** Description used for Doxygen **/
    QString    category; /** Description used for Doxygen **/
} ObjectInfo;

class UAVObjectParser {
public:

    // Functions
    UAVObjectParser();
    QString parseXML(QString & xml, QString & filename);
    int getNumObjects();
    QList<ObjectInfo *> getObjectInfo();
    QString getObjectName(int objIndex);
    quint32 getObjectID(int objIndex);

    ObjectInfo *getObjectByIndex(int objIndex);
    int getNumBytes(int objIndex);
    QStringList all_units;

private:
    QList<ObjectInfo *> objInfo;
    QStringList fieldTypeStrXML;
    QList<int> fieldTypeNumBytes;
    QStringList updateModeStr;
    QStringList updateModeStrXML;
    QStringList accessModeStr;
    QStringList accessModeStrXML;

    QString processObjectAttributes(QDomNode & node, ObjectInfo *info);
    QString processObjectFields(QDomNode & childNode, ObjectInfo *info);
    QString processObjectAccess(QDomNode & childNode, ObjectInfo *info);
    QString processObjectDescription(QDomNode & childNode, QString *description);
    QString processObjectCategory(QDomNode & childNode, QString *category);
    QString processObjectMetadata(QDomNode & childNode, UpdateMode *mode, int *period, bool *acked);
    void calculateID(ObjectInfo *info);
    quint32 updateHash(quint32 value, quint32 hash);
    quint32 updateHash(QString & value, quint32 hash);
};

#endif // UAVOBJECTPARSER_H
